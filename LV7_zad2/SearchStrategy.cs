﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_zad2
{
    abstract class SearchStrategy
    {
        public abstract void Search(double[] array,double number);
    }
}
