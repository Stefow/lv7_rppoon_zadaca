﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_zad2
{
    class LinearSearch:SearchStrategy
    {
        public override void Search(double[] array,double searchNumber)
        {
            bool founded = false;
            int arraySize = array.Length;
            int i = 0;
            for (i = 0; i <arraySize; i++)
            {
                if(searchNumber==array[i])
                {
                    founded = true;
                    break;
                }
            }
            if (founded)
            {
                Console.WriteLine("Pronađen broj je na: " + i.ToString() + ". indeksu");
            }
            else
            {
                Console.WriteLine("Nije pronađen taj broj");
            }
        }
    }
}
