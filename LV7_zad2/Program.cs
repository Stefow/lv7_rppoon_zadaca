﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            LinearSearch linear = new LinearSearch();
            double[] b = { 2, 1, 3, 4, 9, 6, 10, 8, 5, 7 };
            NumberSequence a1 = new NumberSequence(b);
            Console.WriteLine("Linearno pretrazivanje :");
            a1.SetSearchStrategy(linear);
            a1.SetSearchNumber(6);
            a1.Search();

            SequentialSearch seq = new SequentialSearch();
            NumberSequence a2 = new NumberSequence(b);
            Console.WriteLine("SequentialSearch :");
            a2.SetSearchStrategy(seq);
            a2.SetSearchNumber(6);
            a2.Search();
            Console.ReadLine();
        }
    }
}
