﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_zad2
{
    class SequentialSearch : SearchStrategy
    {
        public override void Search(double[] array, double searchNumber)
        {
            
            int flag = 0;
            int i = 0;
            for (i = 0; i < array.Length; i++)
            {
                if (array[i] == searchNumber) 
                {
                    flag = 1; 
                    break; 
                }
            }
            if (flag%2==1)
            {
                Console.WriteLine( "Pronađen broj je na: "+i.ToString()+". indeksu");
            }
            else
            {
                Console.WriteLine("Nije pronađen taj broj" );
            }
        }
    }
}
