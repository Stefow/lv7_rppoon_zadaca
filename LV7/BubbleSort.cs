﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7
{
    class BubbleSort : SortStrategy
    {
        public override void Sort(double[] array)
        {
            double temp;
            int arraySize = array.Length;
            for (int j = 0; j <= arraySize - 2; j++)
            {
                for (int i = 0; i <= arraySize - 2; i++)
                {
                    if (array[i] > array[i + 1])
                    {
                        temp = array[i + 1];
                        array[i + 1] = array[i];
                        array[i] = temp;
                    }
                }
            }
        }
    }
}
